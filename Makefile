.PHONY: all
all:

.PHONY: help
help:
	echo "Call 'make tidy' to verify the coding convention"

.PHONY: tidy
tidy: tools/tidy
	$< --only-changed
	@echo "[make] Tidy called over modified/new files only. For a full run use make tidy-full"

.PHONY: tidy-full
tidy-full: tools/tidy
	$<

