use base "installedtest";
use strict;
use testapi;

sub run {
    sleep 120;
}

sub test_flags {
    return { 'ignore_failure' => 1 };
}

1;

# vim: set sw=4 et:
