use base "installedtest";
use strict;
use testapi;
use utils;

# This test disables unattended updates, which causes unnecessary network traffic,
# workload peaks and desktop notifications at inconvenient moments.

sub run {
    my $self = shift;
    # Become root
    $self->root_console(tty => 3);
    console_loadkeys_us;
    script_run "systemctl mask packagekit";    # Turn off packagekit, even if it is a dependency
    script_run "systemctl stop packagekit";    # Stop the currently running packagekit
    script_run "systemctl stop unattended-upgrades";
    # If the image is slightly older and the test is not started fast enough,
    # there will already be updates to be downloaded, which requires a longer timeout
    script_run "apt-get update", timeout => 60;
    # Switch back to desktop.
    desktop_vt();
}

# If this test fails, the others will probably start failing too,
# so there is no need to continue.
sub test_flags {
    return {fatal => 1};
}

1;

# vim: set sw=4 et:
