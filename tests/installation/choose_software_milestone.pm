# Copyright (C) 2014-2017 SUSE LLC
# Copyright (C)      2017 Philip Hands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

use base "debianinstallertest";
use strict;
use testapi;
use List::MoreUtils qw(first_index);

sub run {
    # FIXME -- this should be a bit more definite than fiddling with timeouts
    my $cs_timeout = check_var('FLAVOR', 'mini-iso') ? 600 : 60 ;

    assert_screen( 'ChooseSoftware', $cs_timeout );
}

sub test_flags {
    return { fatal => 1, milestone => 1 };
}

1;
# vim: set sw=4 et:
