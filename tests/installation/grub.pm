# Copyright (C) 2014-2017 SUSE LLC
# Copyright (C)      2017 Philip Hands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

use base "debianinstallertest";
use strict;
use testapi;
use utils qw(console_loadkeys_us);

sub run {
    my $self = shift;
    my @tags = qw(blankScreen minissdpd grub_JustHitReturnHere DefaultDisplayManager ForceUefiInstallation InstallGRUB GRUBEnterDev CompleteInstall InstallationStepFailed);
    my $nochange   = 0;
    my $seen_mandb = 0;
    push(@tags, "configuring_mandb") if (check_var("DEBUG_AT_MANDB", 1));

    while ($nochange < 10) {

        if (wait_screen_change(sub { sleep 5; }, 55, similarity_level => 95)) {
            diag 'wait_screen_change: changed :-)';
            $nochange = 0;
        }
        else {
            $nochange++;
            diag "wait_screen_change: no change  :-(  [$nochange]";
        }

        if (check_screen \@tags, 5) {
            if (match_has_tag("InstallationStepFailed")) {
                die "found 'InstallationStepFailed'";
            }
            elsif (match_has_tag("blankScreen")) {
                record_soft_failure 'deb#787279: a screen-saver in the installer is mostly pointless';
                mouse_set(800, 820);
                sleep 1;
                mouse_hide;
            }
            elsif (match_has_tag('configuring_mandb')) {
                if (!$seen_mandb) {
                    $seen_mandb = 1;
                    # there's nothing to do here, but it's a good place
                    # to put hacky debuging because it's on-screen for a while

                    # track down grub install failure issue
                    $self->root_console(tty => 3);

                    console_loadkeys_us;

                    script_run "sed -i -e '2,6s/# \\(export DEBCONF_DEBUG\\)/\\1/' /usr/bin/grub-installer";
                    assert_script_run 'head /usr/bin/grub-installer > /dev/ttyS0';
                    assert_script_run 'chroot /target od -A x -t x1z -v /dev/vda | head -1000 > /tmp/dev_vda_dump.txt';

                    send_key "ctrl-alt-f5";    # FIXME: would be nice to make $self->desktop_vt() work here
                }
            }
            elsif (match_has_tag('grub_JustHitReturnHere')) {
                # this is handy for adding workarounds for stuff like #981554
                send_key 'ret';
            }
            elsif (match_has_tag('minissdpd')) {
                send_key 'ret';
            }
            elsif (match_has_tag('DefaultDisplayManager')) {
                send_key 'down';
                send_key 'ret';
            }
            elsif (match_has_tag('ForceUefiInstallation')) {
                send_key 'down';
                send_key 'ret';
            }
            else {
                last;
            }
        }
    }

    # the last check after previous intervals must be fatal
    assert_screen \@tags;

    # we have a race-condition with the blank screen :-/
    if (match_has_tag("blankScreen")) {
        # FIXME -- I think this should be fixed, but let's allow some nice green results for now
        # record_soft_failure 'deb#787279: a screen-saver in the installer is mostly pointless';
        mouse_set(800, 820);
        sleep 1;
        mouse_hide;
        assert_screen \@tags;
    }

    if (match_has_tag("InstallGRUB")) {
        assert_screen 'Yes';
        send_key 'tab' if ('gtk' eq get_var('DI_UI', 'gtk'));
        send_key 'ret';
        assert_screen 'GRUBEnterDev';
    }
    if (match_has_tag("GRUBEnterDev")) {
        if (check_var('DI_UI', 'speech')) {
            send_key '2';
        }
        else {
            send_key 'down';
            assert_screen 'GRUBTargetSelected';
        }
        send_key 'ret';
    }
}

sub test_flags {
    return {fatal => 1};
}

1;
# vim: set sw=4 et:
