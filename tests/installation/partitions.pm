# Copyright (C) 2014-2017 SUSE LLC
# Copyright (C)      2017 Philip Hands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

use base "debianinstallertest";
use strict;
use testapi;

sub run {
    assert_screen [qw(PartitioningMethod AutoPartiton SelectTimezone)], 60;
    if (match_has_tag('SelectTimezone')) {
        # just accept the default for now
        send_key 'ret';
        check_screen [qw(PartitioningMethod AutoPartiton)];
    }
    if (match_has_tag('AutoPartiton')) {
        send_key ('gtk' eq (get_var('DI_UI', 'gtk')) ? 'down' : 'tab');
        send_key 'ret';
    }
    else {
        assert_screen 'PartitioningMethod';
        send_key '1' if check_var('DI_UI', 'speech');
        send_key 'ret';

        assert_screen 'SelectDiskToPartition';
        send_key 'ret';

        assert_screen 'PartitioningScheme';
        if (check_var('PARTITIONING_SCHEME', 'home')) {
            send_key 'down';
        }
        send_key 'ret';

        assert_screen 'FinishPartitioning';
        send_key 'ret';

        # prompt about Writing Partitions to disk:
        assert_screen 'No';
        if (check_var('DI_UI', 'speech')) {
            send_key '1';
        }
        else {
            send_key ('gtk' eq (get_var('DI_UI', 'gtk')) ? 'down' : 'tab');
            assert_screen 'Yes';
        }
        send_key 'ret';
    }
}

sub test_flags {
    return { fatal => 1 };
}

1;
# vim: set sw=4 et:
