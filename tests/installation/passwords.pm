# Copyright (C) 2014-2017 SUSE LLC
# Copyright (C)      2017 Philip Hands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

use base "debianinstallertest";
use strict;
use testapi;

sub run {
    if (!check_var('DISTRI', 'kali')) {  # kali doesn't setup a root password
        assert_screen 'rootPassword';
        my $root_password = get_var("ROOT_PASSWORD", "weakpassword");
        type_password $root_password unless ($root_password eq "LEAVE-BLANK");
        if ('gtk' eq get_var('DI_UI', 'gtk')) {
            send_key 'tab';
            send_key 'tab';
        } else {
            send_key 'ret';
            assert_screen 'rootPasswordAgain';
        }
        type_password $root_password unless ($root_password eq "LEAVE-BLANK");
        send_key 'ret';
    }
    
	assert_screen 'NameOfUser';
	type_string get_var("USER_NAME", "Testy McTestface");
	send_key 'ret';
	send_key 'ret';
	
	assert_screen 'UserPassword';
	my $user_password = get_var("USER_PASSWORD", "weakpassword");
	type_password $user_password;
    if ('gtk' eq get_var('DI_UI', 'gtk')) {
	    send_key 'tab';
	    send_key 'tab';
    } else {
        send_key 'ret';
	    assert_screen 'UserPasswordAgain';
	}
	type_password $user_password;
	send_key 'ret';
}

sub test_flags {
    return { fatal => 1 };
}

1;
# vim: set sw=4 et:
