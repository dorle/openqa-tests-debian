use base "installedtest";
use strict;
use testapi;
use utils;

# This sets the KDE desktop background to plain black, to avoid
# needle match problems caused by transparency.

sub run {
    my $self = shift;

    # Plasma in testing does a welcome screen as of 2023-10, so deal with it
    handle_welcome_screen if (check_var('DESKTOP', 'kde'));

    solidify_wallpaper;
    # get rid of unwanted notifications that interfere with tests
    click_unwanted_notifications;
}

sub test_flags {
    return {fatal => 1, milestone => 1};
}


1;

# vim: set sw=4 et:
