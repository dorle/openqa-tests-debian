use base "debianinstallertest";
use strict;
use lockapi;
use testapi;
use utils;
use debianinstaller;
use autotest;

sub run {
    my $self = shift;

    send_key 'home';
    assert_screen 'language_subprompt';
}


sub test_flags {
    return {fatal => 1, milestone => 1};
}

1;

# vim: set sw=4 et:
