#+Title: openqa .debian.net
#+Subtitle: automated testing of debian-installer ... and beyond
#+Author: Philip Hands
#+EMAIL: phil@hands.com

#+REVEAL_INIT_OPTIONS: width:1100, height:800, margin: 0.2, minScale:0.2, maxScale:2.5
#+OPTIONS: toc:nil num:nil
#+REVEAL_THEME: serif
#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="openqa.debian.net debconf21 presentation">
#+REVEAL_POSTAMBLE: <p> Created by Philip Hands. </p>
#   +REVEAL_PLUGINS: (markdown notes)
#   +REVEAL_HLEVEL: 2
#   +REVEAL_TITLE_SLIDE: <h1 class="title">%t</h1><h2 class="author">%a</h2><p class="date">%d</p>


#  to get to the org-reveal menu:  C-c C-e    ( R R  does the export )

# * Table of Contents
#  ...

* A Quick Guided Tour

http://openqa.debian.net/

* Questions and Answers
#+ATTR_REVEAL: :frag (appear)
- [[https://openqa.debian.net/]]
- [[https://salsa.debian.org/debian/openqa]]
- [[https://salsa.debian.org/qa/openqa/openqa-tests-debian/]]
- [[https://debconf21.debconf.org/talks/30-branch2repo-enabling-casual-contributions-to-debian-installer/]]
- Email: **phil@hands.com**
- IRC:   **fil** /on OFTC etc./
