#+TITLE: Todo

* Bugs:
** speech install tends to skip past first return
should try looking for the second prompt, and then only bothering with speech
from there on in
** speech test isn't letting us do more than one recording -- why?

* TODOs

** TODO put /usr/local/bin/openqa-hook_success in ansible, and the matching configuration of hooks openqa.ini
** TODO make JOBSTOCLONE be a list
or perhaps work out a way of querying an existing build, and running all the jobs for it as clones

** maybe add console_loadkeys_us to lib/utils.pm, thus:

    @@ -226,6 +226,7 @@ sub _console_login_finish {
                 record_soft_failure "It looks like profile sourcing failed";
             }
         }
    +    console_loadkeys_us;
     }
 
     # this subroutine handles logging in as a root/specified user into console
** TODO in hostname.pm (or kernel_mismatch.pm) we've seen that SCSI CDs don't (always?) work -- probably ought to fix that
** TODO add some Librem-5 tests
** TODO debian-edu server tests, when the new stuff is ready for testing
