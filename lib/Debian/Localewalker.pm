package Localewalker;
use Moose;

# perhaps this should be an array of locales, rather than just a number
has locales    => (isa => 'Int', is => 'rw', default => sub { 1 });

sub register_tests {
    my $self = shift;
    my $localewalk_args;

    for my $downs (0..$self->locales) {
        # my $final_path_ref  = [@{$path_to_here_ref}, $counter];
        # my $final_path_name = [@{$path_to_here_ref}, 'last'];
        my $testname        = 'locale_' . "$downs";
        $localewalk_args              = OpenQA::Test::RunArgs->new();
        # $localewalk_args->{needle}    = undef;
        # $localewalk_args->{menu_path} = $final_path_ref;
        $localewalk_args->{downs} = $downs;
        if ($downs == $self->locales) {
            $localewalk_args->{last} = 1;
        }

        autotest::loadtest('tests/locale_walk_once.pm', name => $testname, run_args => $localewalk_args);
    }
    return;
}

sub BUILD {
    my $self = shift;

    use testapi;

    if (check_var('DI_UI', 'text')) {
        $self->locales(60);
    }
    else {
        $self->locales(77);
    }

    return;
}

#no Moo;
1;
